package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.api.SeleniumBase;
import com.framework.design.ProjectMethods;

public class ACMESearch extends ProjectMethods{
   
	
	public ACMESearch() {
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how= How.ID,using="vendorTaxID") WebElement eleVendorID;
	@FindBy(how= How.ID,using="buttonSearch") WebElement eleSearch;
	
	
	public ACMESearch enterVendorID() {
		clearAndType(eleVendorID, "RO094782");
		return this;
	}
	
	public ACMEResults searchVendors() {
		click(eleSearch);
		return new ACMEResults();
	}
	
	
}
