package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class HomePage extends ProjectMethods{

	public HomePage() {
       PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.XPATH,using="//a[contains(text(),'CRM/SFA')]") WebElement eleCRMSFA;
	@FindBy(how = How.CLASS_NAME,using="decorativeSubmit") WebElement eleLogOut;
	public MyHomePage clickCRMSFA() {
		click(eleCRMSFA);
		return new MyHomePage(); 
	}
	
	public LoginPage clickLogout() {
		click(eleLogOut);
		return new LoginPage(); 
	}
	


}
















