package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindLeadsPage extends ProjectMethods{
	
	public FindLeadsPage() {
	       PageFactory.initElements(driver, this);
		}
	
		@FindBy(how = How.XPATH,using="//div[@class='x-form-item x-tab-item']//input[@name='firstName']") WebElement eleFirstName;
		@FindBy(how = How.XPATH,using="//button[contains(text(),'Find Leads')]") WebElement eleFindLeads;
		@FindBy(how = How.XPATH,using="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a[@class='linktext'][1]") WebElement eleFirstLeadResult;
		@FindBy(how = How.XPATH,using="//span[contains(text(),'Email')]") WebElement eleEmail;
		@FindBy(how = How.NAME,using="emailAddress") WebElement eleEmailAdd;
		
		public FindLeadsPage searchFirstName(String data) {
			clearAndType(eleFirstName, data);
			return this; 
		}
		
		public FindLeadsPage clickEmail() {
			click(eleEmail);
			return this; 
		}
		
		public FindLeadsPage searchEmail(String data) {
			clearAndType(eleEmailAdd, data);
			return this; 
		}
		
		public FindLeadsPage clickSearchFindLead() {
			click(eleFindLeads);
			return this; 
		}
		
		public ViewLeadPage clickFirstLeadResult() {
			click(eleFirstLeadResult);
			return new ViewLeadPage();
		}
				
	

}
