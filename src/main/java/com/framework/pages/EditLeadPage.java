package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class EditLeadPage extends ProjectMethods{
	
	public EditLeadPage() {
	       PageFactory.initElements(driver, this);
		}
		@FindBy(how = How.ID,using="updateLeadForm_companyName") WebElement eleCompName;
		@FindBy(how = How.XPATH,using="//input[@value='Update']") WebElement eleUpdateLead;
		
		public EditLeadPage updateCompanyName(String data) {
			clearAndType(eleCompName, data);
			return this; 
		}
		
		public ViewLeadPage clickUpdateLead() {
			click(eleUpdateLead);
			return new ViewLeadPage();
		}
		
					
	

}
