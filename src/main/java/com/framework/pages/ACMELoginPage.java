package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.api.SeleniumBase;
import com.framework.design.ProjectMethods;

public class ACMELoginPage extends ProjectMethods{

	public ACMELoginPage() {
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how= How.ID,using="email") WebElement eleEmail;
	@FindBy(how= How.ID,using="password") WebElement elePassword;
	@FindBy(how= How.ID,using="buttonLogin") WebElement eleLogin;
	
	
	public ACMELoginPage enterEmail() {
		clearAndType(eleEmail, "mathivathani019@gmail.com");
		return this;
	}
	
	public ACMELoginPage enterPassword() {
		clearAndType(elePassword, "SelTam@2015");
		return this;
	}
	
	public ACMEDashboard clickLogin() {
		click(eleLogin);
		return new ACMEDashboard();
	}
}
