package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class DuplicateLead extends ProjectMethods{
	
	public DuplicateLead() {
	       PageFactory.initElements(driver, this);
		}
		
		@FindBy(how = How.XPATH,using="//input[contains(@value,'Create Lead')]") WebElement eleDuplicateLead;
		

		public ViewLeadPage clickDupLead() {
			click(eleDuplicateLead);
			return new ViewLeadPage();
		}
		
					
	

}
