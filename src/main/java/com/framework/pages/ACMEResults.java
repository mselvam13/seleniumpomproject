package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.api.SeleniumBase;
import com.framework.design.ProjectMethods;

public class ACMEResults extends ProjectMethods{
   
	
	public ACMEResults() {
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how= How.XPATH,using="//tr[2]/td[1]") WebElement eleVendorName;
		
	
	public ACMEResults dispVendorName() {
		String elementText = getElementText(eleVendorName);
		System.out.println(elementText);
		return this;
	}
	
	
	
}
