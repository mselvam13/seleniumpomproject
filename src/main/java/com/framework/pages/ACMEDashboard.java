package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.api.SeleniumBase;
import com.framework.design.ProjectMethods;

public class ACMEDashboard extends ProjectMethods{
   
		
	public ACMEDashboard() {
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how= How.XPATH,using="//button[text()=' Vendors']") WebElement eleVendors;
	@FindBy(how= How.XPATH,using="//a[text()='Search for Vendor']") WebElement eleSearchVendors;
	
	
	public ACMEDashboard moveToVendors() {
		moveToElementAct(eleVendors);
		return this;
	}
	
	public ACMESearch moveToSearchVendors() {
		moveToElementActClick(eleSearchVendors);
		return new ACMESearch();
	}
	
	
}
