package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	public ViewLeadPage() {
       PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID,using="viewLead_firstName_sp") WebElement eleFirstName;
	@FindBy(how = How.ID,using="viewLead_companyName_sp") WebElement eleCompanyName;
	@FindBy(how = How.XPATH,using="//a[contains(text(),'Edit')]") WebElement eleEditLead;
	@FindBy(how = How.XPATH,using="//a[contains(text(),'Duplicate Lead')]") WebElement eleDuplicateLead;
	
	
	public ViewLeadPage verifyFirstName(String data) {
		String firstNameText = getElementText(eleFirstName);
		if(firstNameText.equalsIgnoreCase(data)) {
			System.out.println("Lead is created");
		}
		return this; 
	}
	
	public ViewLeadPage verifyCompanyName(String data) {
		String companyNameText = getElementText(eleCompanyName);
		if(companyNameText.equalsIgnoreCase(data)) {
			System.out.println("Company Name is verified");
		}
		return this; 
	}
	
	public EditLeadPage clickEditLead()
	{
	click(eleEditLead);
	return new EditLeadPage();
	}
	
	public DuplicateLead clickDuplicateLead()
	{
	click(eleDuplicateLead);
	return new DuplicateLead();
	}

}
















