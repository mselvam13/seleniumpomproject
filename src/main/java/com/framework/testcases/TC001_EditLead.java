package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_EditLead extends ProjectMethods {
	
	@BeforeTest
	public void initExtentReports() {
		  testCaseName="TC001_EditLead";
		  testDescription="Editing lead";
		  testNodes="Edit Lead";
		  author="Mathivathani";
		  category="Edit Lead Module testing";
		  dataSheetName="TC001_EditLead";
		}
	
	@Test(dataProvider="fetchData")
	public void createLead(String uname,String password,String fname,String compName) {
		new LoginPage()
		.enterUsername(uname)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLead()
		.searchFirstName(fname)
		.clickSearchFindLead()
		.clickFirstLeadResult()
		.clickEditLead()
		.updateCompanyName(compName)
		.clickUpdateLead()
		.verifyCompanyName(compName);
		
	}

}
