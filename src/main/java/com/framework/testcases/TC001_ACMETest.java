package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.api.SeleniumBase;
import com.framework.design.ProjectMethods;
import com.framework.pages.ACMELoginPage;

public class TC001_ACMETest extends ProjectMethods {
	
	@BeforeTest
	public void initExtentReports() {
		testCaseName="TC001_ACMETest";
		  testDescription="Search Vendors";
		  testNodes="Search vendor";
		  author="Mathivathani";
		  category="Search Vendor Module testing";
				}
		
	@Test
	public void searchVendorsACME() {
		new ACMELoginPage()
		.enterEmail()
		.enterPassword()
		.clickLogin()
		.moveToVendors()
		.moveToSearchVendors()
		.enterVendorID()
		.searchVendors()
		.dispVendorName();
	}

}
