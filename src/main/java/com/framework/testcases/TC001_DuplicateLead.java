package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_DuplicateLead extends ProjectMethods {
	
	@BeforeTest
	public void initExtentReports() {
		testCaseName="TC001_DuplicateLead";
		  testDescription="Duplicate lead";
		  testNodes="Duplicate Lead";
		  author="Mathivathani";
		  category="Duplicate Lead Module testing";
		  dataSheetName="TC001_DuplicateLead";
		}
	
	@Test(dataProvider="fetchData")
	public void createLead(String uname,String password,String emailAddress) {
		new LoginPage()
		.enterUsername(uname)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLead()
		.clickEmail()
		.searchEmail(emailAddress)
		.clickSearchFindLead()
		.clickFirstLeadResult()
		.clickDuplicateLead()
		.clickDupLead();
				
	}

}
