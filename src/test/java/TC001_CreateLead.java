

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_CreateLead extends ProjectMethods {
	
	@BeforeTest
	public void initExtentReports() {
	testCaseName="TC001_CreateLead";
	testDescription="Creating lead";
	testNodes="Create Lead";
	author="Mathivathani";
	category="Create Lead Module testing";
	dataSheetName="TC001_CreateLead";
	}
	
	@Test(dataProvider="fetchData")
	public void createLead(String uname,String password,String compName,String firstName,String lastName) {
		new LoginPage()
		.enterUsername(uname)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLead()
		.enterCompName(compName)
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.clickCreateLead()
		.verifyFirstName(firstName);
	}

}
