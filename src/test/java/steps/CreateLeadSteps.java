/*package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.But;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadSteps {
	ChromeDriver driver;
	
	@Given("Invoke Browser")
	public void invokeBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}

	@And("Maximize the browser")
	public void maximizeTheBrowser() {
		driver.manage().window().maximize();
	}

	@And("Set the Timeout")
	public void setTheTimeout() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@And("Enter the URL")
	public void enterTheURL() {
		driver.get("http://leaftaps.com/opentaps");
	}

	@And("Enter the Username")
	public void enterTheUsername() {
	    driver.findElementById("username").sendKeys("DemoSalesManager");
	}

	@And("Enter the Password")
	public void enterThePassword() {
	    driver.findElementById("password").sendKeys("crmsfa");
	}

	@And("Click Login")
	public void clickLogin() {
	    driver.findElementByClassName("decorativeSubmit").click();
	}

	@And("Click CRMSFA")
	public void clickCRMSFA() {
	   driver.findElementByXPath("//a[contains(text(),'CRM/SFA')]").click();
	}

	@And("Click Leads")
	public void clickLeads() {
	    driver.findElementByXPath("//a[contains(text(),'Leads')]").click();
	}

	@And("Click Create Lead")
	public void clickCreateLead() {
	    driver.findElementByXPath("//a[contains(text(),'Create Lead')]").click();
	}

	@And("Enter Company Name as (.*)")
	public void enterCompanyName(String compName) {
	    driver.findElementById("createLeadForm_companyName").sendKeys(compName);
	}

	@And("Enter First Name as (.*)")
	public void enterFirstName(String fname) {
	    driver.findElementById("createLeadForm_firstName").sendKeys("Mathivathani");
	}
	

	@And("Enter Last Name as (.*)")
	public void enterLastName(String lname) {
		 driver.findElementById("createLeadForm_lastName").sendKeys("Selvam");
	}

	@When("Submit Create Lead")
	public void submitCreateLead() {
	    driver.findElementByName("submitButton").click();
	}

	@Then("verify")
	public void verify() {
	    System.out.println("Verified");
	}
    
	@But("Create lead fails")
	public void failCreateLead()
	{
		System.out.println("Lead is not created");
	}



}
*/