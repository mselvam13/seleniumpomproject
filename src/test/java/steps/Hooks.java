package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

	@Before
	public void beforeScenario(Scenario sc) {
		System.out.println("Scenario Name: "+sc.getName());
		System.out.println("ID: "+sc.getId());
	}
	
	@After
    public void afterScenario(Scenario sc) {
		System.out.println("Status: "+sc.getStatus());
		
	}
}
