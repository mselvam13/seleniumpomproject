package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import com.framework.design.ProjectMethods;

import cucumber.api.java.en.And;
import cucumber.api.java.en.But;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadSteps2{
	ChromeDriver driver;
	
	@Given("Invoke Browser")
	public void invokeBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}

	@And("Maximize the browser")
	public void maximizeTheBrowser() {
		driver.manage().window().maximize();
	}

	@And("Set the Timeout")
	public void setTheTimeout() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@And("Enter the URL")
	public void enterTheURL() {
		driver.get("http://leaftaps.com/opentaps");
	}

	@And("Enter the Username with (.*)")
	public void enterTheUsername(String uname) {
	    driver.findElementById("username").sendKeys(uname);
	}

	@And("Enter the Password with (.*)")
	public void enterThePassword(String pwd) {
	    driver.findElementById("password").sendKeys(pwd);
	}

	@And("Click Login")
	public void clickLogin() {
	    driver.findElementByClassName("decorativeSubmit").click();
	}

	@And("Click CRMSFA")
	public void clickCRMSFA() {
	   driver.findElementByXPath("//a[contains(text(),'CRM/SFA')]").click();
	}

	@And("Click Leads")
	public void clickLeads() {
	    driver.findElementByXPath("//a[contains(text(),'Leads')]").click();
	}

	@And("Click Create Lead")
	public void clickCreateLead() {
	    driver.findElementByXPath("//a[contains(text(),'Create Lead')]").click();
	}

	@And("Enter Company Name with (.*)")
	public void enterCompanyName(String compName) {
	    driver.findElementById("createLeadForm_companyName").sendKeys(compName);
	}

	@And("Enter First Name with (.*)")
	public void enterFirstName(String fname) {
	    driver.findElementById("createLeadForm_firstName").sendKeys(fname);
	}
	

	@And("Enter Last Name with (.*)")
	public void enterLastName(String lname) {
		 driver.findElementById("createLeadForm_lastName").sendKeys(lname);
	}

	@When("Submit Create Lead")
	public void submitCreateLead() {
	    driver.findElementByName("submitButton").click();
	}

	@Then("verify")
	public void verify() {
	    System.out.println("Verified");
	}
    
	@But("Create lead fails")
	public void failCreateLead()
	{
		System.out.println("Lead is not created");
	}



}
