Feature: CreateLead2 for LeafTaps 

Background: 
	Given Invoke Browser 
	And Maximize the browser 
	And Set the Timeout 
	And Enter the URL 

@Smoke	
Scenario Outline: PositiveScenario  
 Given Enter the Username with <uname> 
	And Enter the Password with <pwd> 
	And Click Login 
	And Click CRMSFA 
	And Click Leads 
	And Click Create Lead 
	And Enter First Name with <fname> 
	And Enter Last Name with <lname> 
	When Submit Create Lead 
	Then Create lead fails 
	
	Examples: 
		| uname |pwd  |fname|lname|
		|DemoSalesManager|crmsfa|  Mathivathani | Selvam |
		|DemoSalesManager|crmsfa| Ishwarya | Sekar |
		|DemoSalesManager|crmsfa|  Iswarya | Raj |
		

@Sanity		
Scenario Outline: NegativeScenario  
 Given Enter the Username with <uname> 
	And Enter the Password with <pwd> 
	And Click Login 
	And Click CRMSFA 
	And Click Leads 
	And Click Create Lead 
	And Enter Company Name with <cmpName> 
	And Enter First Name with <fname> 
	And Enter Last Name with <lname> 
	When Submit Create Lead 
	Then Create lead fails 
	
	Examples: 
		| uname |pwd  | cmpName |fname|lname|
		|DemoSalesManager|crmsfa| TCS | Mathivathani | Selvam |
		|DemoSalesManager|crmsfa| WIPRO | Ishwarya | Sekar |
		|DemoSalesManager|crmsfa| TM | Iswarya | Raj |