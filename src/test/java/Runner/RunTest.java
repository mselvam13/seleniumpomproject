package Runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/java/features/CreateLead2.feature",
                 glue="steps",
                 monochrome=true,
                 tags="@Sanity",
                 plugin= {"pretty","html:reports/CucumberBasicReports"})
              
public class RunTest {

}
