Feature: CreateLead for LeafTaps

Background:
Given Invoke Browser
And Maximize the browser
And Set the Timeout
And Enter the URL
And Enter the Username 
And Enter the Password
And Click Login
And Click CRMSFA
And Click Leads
And Click Create Lead

Scenario: Positive Scenario
Given Enter Company Name as TCS
And Enter First Name as Mathivathani
And Enter Last Name as Selvam
When Submit Create Lead
Then verify


Scenario: Negative Scenario
Given Enter First Name as Ishwarya
And Enter Last Name as Sekar
When Submit Create Lead
But Create lead fails

